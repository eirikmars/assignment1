const customer = {
    name: "Eirik Marstrander",
    bank: 0,
    pay: 0,
    loan: 0
}

const nameEl = document.getElementById("bankName")
const valueEl = document.getElementById("bankValue")
const repayLoanEl = document.getElementById("repay-loan")
const selectBoxEl = document.getElementById("select-box")
const featuresEl = document.getElementById("features")
const laptopNameEl = document.getElementById("laptop-name")
const descriptionEl = document.getElementById("description")
const imageEl = document.getElementById("img")
const laptopPriceEl = document.getElementById("laptop-price")
const payEl = document.getElementById("work-pay")
const loanEl = document.getElementById("loan")

let computers

// update website with customer values and computers
valueElUpdate()
payElUpdate()
getComputers()
nameEl.innerText = customer.name

function valueElUpdate() {
    valueEl.innerHTML = Number(customer.bank) + customer.loan
}

function payElUpdate() {
    payEl.innerText = customer.pay
}

function work() {
    customer.pay += 100
    payElUpdate()
}

function getLoan(){
    if (customer.loan > 0) {
        window.alert("You may not have two loans at once.")
        return
    } 
    
    let loan = Number(prompt("How much loan do you need?"))
    
    if (loan > Number(customer.bank * 2)) {
        window.alert("You cannot get a loan more than double of your bank balance.")
    } else if (!isNaN(loan) && loan > 0) {
        customer.loan += Number(loan)
        valueElUpdate()
        repayLoanEl.style.display = "block"
        loanEl.style.display = "block"
        loanUpdate()
    } else {
        window.alert("Not a valid loan value")
    }
}

function loanUpdate() {
    if(customer.loan == 0) {
        loanEl.style.display = "none"
    } else {
        loanEl.innerText = "Outstanding loan " + customer.loan
    }
}

function repayLoan(){
    if (customer.loan <= customer.pay) {
        customer.pay -= customer.loan
        customer.bank += customer.pay
        customer.bank += customer.loan
        customer.loan = 0
        customer.pay = 0
        repayLoanEl.style.display = "none"
    } else {
        customer.bank += customer.pay
        customer.loan -= customer.pay
        customer.pay = 0
    }
    payElUpdate()
    valueElUpdate()
    loanUpdate()
}

function transferPayToBank(){
    if (customer.loan > 0){
        if (customer.loan > customer.pay*0.1) {
            customer.loan -= customer.pay*0.1
        } else {
            customer.pay -= customer.loan
            customer.loan = 0
        }
    }
    customer.bank += customer.pay
    customer.pay = 0
    valueElUpdate()
    loanUpdate()
    payElUpdate()
}

async function getComputers() {
    try {
        const response = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
        computers = await response.json()
        for (const computer of computers) {
            const option = document.createElement("option")
            option.innerText = computer.title
            selectBoxEl.add(option)
        }
        updateComputer()
    }catch(error) {
        console.error('Something went wrong', error)
    }
}

/**
 * Updates computer in computer-section
 */
async function updateComputer() {
    let computer = computers[selectBoxEl.selectedIndex]
    featuresEl.innerHTML = ""
    computer.specs.forEach(element => {
        liEl = document.createElement("li")
        liEl.innerText = element
        featuresEl.append(liEl)
    })
    laptopNameEl.innerHTML = computer.title
    descriptionEl.innerHTML = computer.description
    laptopPriceEl.innerHTML = computer.price + " NOK"
    imageEl.src = await getImage(computer)
}

function buyNow() {
    let computer = computers[selectBoxEl.selectedIndex]
    if ((customer.bank + customer.loan) >= computer.price) {
        alert(`You have bought ${computer.title} for ${computer.price} NOK.`)
        customer.bank -= computer.price
        valueElUpdate()
        loanUpdate()
    } else {
        alert(`You don't have the facilities to buy ${computer.title}.`)
    }
}

async function getImage(computer) {
    let img_url
    let mimes=[ "png","jpeg","jpg","gif", "svg"]

    //splits path and mime
    const split = computer.image.split(".");
    //sets initial mime at the front of the array
    [mimes[mimes.indexOf(split[1])] , mimes[0]] = [mimes[0], mimes[mimes.indexOf(split[1])]]    

    let http = new XMLHttpRequest()
    for (let mime of mimes){
        img_url = "https://noroff-komputer-store-api.herokuapp.com/" + split[0] + "." + mime
        http = new XMLHttpRequest()
        http.open('HEAD', img_url, false)
        http.send()
        if(http.status != 404){
            return img_url
        }
    }
}